package entity

// Session contain info about user session
type Session struct {
	UserID   string
	Token    string
	TokenFCM string
}
