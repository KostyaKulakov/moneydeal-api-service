package usecase

import (
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/entity"
)

type StorageRepository interface {
	Register(phone string) (string, error)
	GetIDByPhone(phone string) (string, error)
	UpdateAccountInfo(info *entity.Account) error
	BlockPhone(phone string) error
	UnblockPhone(phone string) error
	GetByPhone(phone string) (*entity.Account, error)
	GetByID(id string) (*entity.Account, error)
}

type StorageSession interface {
	Create(userID, tokenFCM string) (string, error)
	Get(token string) (*entity.Session, error)
	Delete(token string) error
}

// Account perform account use cases
type Account struct {
	storage StorageRepository
	session StorageSession
}

// NewAccountUsecase create instance of AccountUsecase
func NewAccountUsecase(storage StorageRepository, session StorageSession) Account {
	return Account{
		storage: storage,
		session: session,
	}
}

// Register add new account
func (a Account) Register(phone string) (string, error) {
	return a.storage.Register(phone)
}

// GetIDByPhone find account id by phone
func (a Account) GetIDByPhone(phone string) (string, error) {
	return a.storage.GetIDByPhone(phone)
}

// UpdateAccountInfo update account info
func (a Account) UpdateAccountInfo(info *entity.Account) error {
	return a.storage.UpdateAccountInfo(info)
}

// BlockPhone add phone to block list
func (a Account) BlockPhone(phone string) error {
	return a.storage.BlockPhone(phone)
}

// UnblockPhone remove phone from block list
func (a Account) UnblockPhone(phone string) error {
	return a.storage.UnblockPhone(phone)
}

// GetByPhone find account by phone
func (a Account) GetByPhone(phone string) (*entity.Account, error) {
	return a.storage.GetByPhone(phone)
}

// GetByID find account by id
func (a Account) GetByID(id string) (*entity.Account, error) {
	return a.storage.GetByID(id)
}

// GetIDByToken find account id by session token
func (a Account) GetIDByToken(token string) (string, error) {
	s, err := a.session.Get(token)
	if err != nil {
		return "", err
	}

	return s.UserID, nil
}

// CreateToken create new user token
func (a Account) CreateToken(userID, tokenFCM string) (string, error) {
	return a.session.Create(userID, tokenFCM)
}

// DeleteToken remove account session token
func (a Account) DeleteToken(token string) error {
	return a.session.Delete(token)
}
