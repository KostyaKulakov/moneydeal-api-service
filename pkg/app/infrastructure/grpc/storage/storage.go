package storage

import (
	"context"

	"gitlab.com/ZorinArsenij/moneydeal-api-service/internal/config"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/entity"
	service "gitlab.com/ZorinArsenij/moneydeal-database-service/pkg/app/delivery/grpc"
	"google.golang.org/grpc"
)

// Repository process requests to storage
type Repository struct {
	client service.StorageClient
}

// NewRepository create instance of Repository
func NewRepository(cfg config.Config) (Repository, error) {
	conn, err := grpc.Dial(cfg.StorageServiceAddr, grpc.WithInsecure())
	if err != nil {
		return Repository{}, err
	}

	return Repository{
		client: service.NewStorageClient(conn),
	}, nil
}

// Register add new account
func (r Repository) Register(phone string) (string, error) {
	req := &service.Phone{Phone: phone}
	resp, err := r.client.RegisterAccount(context.Background(), req)
	if err != nil {
		return "", err
	}
	return resp.ID, nil
}

// GetIDByPhone find account id by phone
func (r Repository) GetIDByPhone(phone string) (string, error) {
	req := &service.Phone{Phone: phone}
	resp, err := r.client.GetAccountIDByPhone(context.Background(), req)
	if err != nil {
		return "", err
	}
	return resp.ID, nil
}

// UpdateAccountInfo update account info
func (r Repository) UpdateAccountInfo(info *entity.Account) error {
	req := &service.AccountInfo{
		ID:        info.ID,
		FirstName: info.FirstName,
		LastName:  info.LastName,
	}
	_, err := r.client.UpdateAccountInfo(context.Background(), req)
	return err
}

// BlockPhone add phone to block list
func (r Repository) BlockPhone(phone string) error {
	req := &service.Phone{Phone: phone}
	_, err := r.client.BlockPhone(context.Background(), req)
	return err
}

// UnblockPhone remove phone from block list
func (r Repository) UnblockPhone(phone string) error {
	req := &service.Phone{Phone: phone}
	_, err := r.client.UnblockPhone(context.Background(), req)
	return err
}

// GetByPhone find account by phone
func (r Repository) GetByPhone(phone string) (*entity.Account, error) {
	req := &service.Phone{Phone: phone}
	acc, err := r.client.GetAccountByPhone(context.Background(), req)
	if err != nil {
		return nil, err
	}

	return &entity.Account{
		ID: acc.ID,
		AccountInfo: entity.AccountInfo{
			FirstName: acc.FirstName,
			LastName:  acc.LastName,
		},
	}, nil
}

// GetByID find account by id
func (r Repository) GetByID(id string) (*entity.Account, error) {
	req := &service.AccountID{ID: id}
	acc, err := r.client.GetAccountByID(context.Background(), req)
	if err != nil {
		return nil, err
	}

	return &entity.Account{
		ID: acc.ID,
		AccountInfo: entity.AccountInfo{
			FirstName: acc.FirstName,
			LastName:  acc.LastName,
			Phone:     acc.Phone,
		},
	}, nil
}
