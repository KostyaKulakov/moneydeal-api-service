package server

import (
	"context"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/ZorinArsenij/authentication-service/pkg/errors"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/delivery/http/server/key"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/delivery/http/server/response"
)

func (s *Server) Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-Auth")
		if token == "" {
			s.respond(w, nil, http.StatusUnauthorized)
			return
		}

		id, err := s.accountUsecase.GetIDByToken(token)
		if err != nil {
			if err == errors.ErrNotFound {
				s.respond(w, nil, http.StatusUnauthorized)
				return
			}

			s.respond(w, nil, http.StatusInternalServerError)
			return
		}

		ctx := context.WithValue(r.Context(), key.ContextID, id)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (s *Server) AccessLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Add context logger
		log := s.log.WithFields(logrus.Fields{
			"method": r.Method,
			"path":   r.URL.Path,
		})

		// Log received request
		start := time.Now()
		log.WithField("start", start).Info()

		// Wrap ResponseWriter into custom ResponseWriter for getting status code
		lrw := response.NewLoggingResponseWriter(w)
		ctx := context.WithValue(r.Context(), key.ContextLogger, log)
		next.ServeHTTP(lrw, r.WithContext(ctx))

		// Log ending of request
		end := time.Now()
		log.WithFields(logrus.Fields{
			"end":    end,
			"time":   end.Sub(start),
			"status": lrw.Status,
		}).Info()
	})
}
