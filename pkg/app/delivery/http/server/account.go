package server

import (
	"bytes"
	"net/http"

	"google.golang.org/grpc/codes"

	easyjson "github.com/mailru/easyjson"
	"github.com/sirupsen/logrus"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/delivery/http/server/key"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/entity"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/errors"
	"google.golang.org/grpc/status"
)

//go:generate easyjson account.go

const (
	googleURL = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPhoneNumber?key=AIzaSyD0CTl0ZV3y-AjL4hOtnJ5n2I-EJylcBWg"
)

//easyjson:json
type requestLogin struct {
	Phone       string `json:"phone"`
	TokenFCM    string `json:"tokenFCM"`
	TokenGoogle string `json:"google_token"`
	Code        string `json:"code"`
}

//easyjson:json
type responseLogin struct {
	ID    string `json:"id"`
	Token string `json:"token"`
}

//easyjson:json
type responseAccountInfo struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Phone     string `json:"phone,omitempty"`
}

//easyjson:json
type requestUpdate struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

//easyjson:json
type requestGoogle struct {
	TokenGoogle string `json:"sessionInfo"`
	Code        string `json:"code"`
}

//easyjson:json
type responseGoogle struct {
	Phone string `json:"phoneNumber"`
}

func checkGoogleCode(tokenGoogle, code string) (*responseGoogle, error) {
	data := requestGoogle{
		TokenGoogle: tokenGoogle,
		Code:        code,
	}

	body, err := data.MarshalJSON()
	if err != nil {
		return nil,
			err
	}

	resp, err := http.Post(googleURL, "application/json", bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	switch resp.StatusCode {
	case http.StatusOK:
		res := responseGoogle{}
		if err := easyjson.UnmarshalFromReader(resp.Body, &res); err != nil {
			return nil, err
		}

		return &res, nil
	case http.StatusBadRequest:
		return nil, errors.ErrInvalidCode
	default:
		return nil, errors.ErrRequestFailed
	}
}

// HandleAccountUpdate update account info
func (s *Server) HandleAccountUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log := r.Context().Value(key.ContextLogger).(*logrus.Entry)
		authID := r.Context().Value(key.ContextID).(string)

		var info requestUpdate
		if err := s.decode(r, &info); err != nil {
			log.Error(err)
			s.respond(w, nil, http.StatusUnprocessableEntity)
			return
		}

		newInfo := entity.Account{
			ID: authID,
			AccountInfo: entity.AccountInfo{
				FirstName: info.FirstName,
				LastName:  info.LastName,
			},
		}

		if err := s.accountUsecase.UpdateAccountInfo(&newInfo); err != nil {
			log.Error(err)
			s.respond(w, nil, http.StatusInternalServerError)
			return
		}

		s.respond(w, nil, http.StatusOK)
	}
}

// HandleAccountInfo get account info
func (s *Server) HandleAccountInfo() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log := r.Context().Value(key.ContextLogger).(*logrus.Entry)
		authID := r.Context().Value(key.ContextID).(string)

		acc, err := s.accountUsecase.GetByID(authID)
		e, _ := status.FromError(err)
		switch {
		case e.Code() == codes.NotFound:
			s.respond(w, nil, http.StatusNotFound)
			return
		case e != nil:
			log.Error(err)
			s.respond(w, nil, http.StatusInternalServerError)
			return
		}

		resp := responseAccountInfo{
			ID:        acc.ID,
			FirstName: acc.FirstName,
			LastName:  acc.LastName,
			Phone:     acc.Phone,
		}
		s.respond(w, resp, http.StatusOK)
	}
}

// HandleLogin login user and create new account if needed
func (s *Server) HandleLogin() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log := r.Context().Value(key.ContextLogger).(*logrus.Entry)

		var data requestLogin
		if err := s.decode(r, &data); err != nil {
			s.respond(w, nil, http.StatusUnprocessableEntity)
			return
		}

		googleResp, err := checkGoogleCode(data.TokenGoogle, data.Code)
		if err != nil {
			log.Error(err)

			switch err {
			case errors.ErrInvalidCode:
				s.respond(w, nil, http.StatusBadRequest)
			default:
				s.respond(w, nil, http.StatusInternalServerError)
			}

			return
		}

		if googleResp.Phone != data.Phone {
			log.Error("phone numbers do not match")
			s.respond(w, nil, http.StatusBadRequest)
			return
		}

		id, err := s.accountUsecase.GetIDByPhone(data.Phone)
		e, _ := status.FromError(err)
		switch {
		case e.Code() == codes.NotFound:
			id, err := s.accountUsecase.Register(data.Phone)
			if err != nil {
				log.Errorf("creation new account failed: %s", err)
				s.respond(w, nil, http.StatusInternalServerError)
				return
			}

			token, err := s.accountUsecase.CreateToken(id, data.TokenFCM)
			if err != nil {
				s.respond(w, nil, http.StatusUnauthorized)
				return
			}

			s.respond(w, responseLogin{ID: id, Token: token}, http.StatusCreated)
			return
		case e != nil:
			log.Errorf("checking phone existence failed: %s", err)
			s.respond(w, nil, http.StatusInternalServerError)
			return
		}

		token, err := s.accountUsecase.CreateToken(id, data.TokenFCM)
		if err != nil {
			s.respond(w, nil, http.StatusUnauthorized)
			return
		}
		s.respond(w, responseLogin{ID: id, Token: token}, http.StatusOK)
	}
}
