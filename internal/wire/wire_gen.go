// Code generated by Wire. DO NOT EDIT.

//go:generate wire
//+build !wireinject

package wire

import (
	"gitlab.com/ZorinArsenij/moneydeal-api-service/internal/config"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/delivery/http/server"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/infrastructure/grpc/session"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/infrastructure/grpc/storage"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/pkg/app/usecase"
)

// Injectors from wire.go:

func InitializeServer(cfg config.Config) (server.Server, error) {
	repository, err := storage.NewRepository(cfg)
	if err != nil {
		return server.Server{}, err
	}
	sessionRepository, err := session.NewRepository(cfg)
	if err != nil {
		return server.Server{}, err
	}
	account := usecase.NewAccountUsecase(repository, sessionRepository)
	serverServer := server.New(account)
	return serverServer, nil
}
