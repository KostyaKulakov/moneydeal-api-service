package main

import (
	"flag"
	"log"

	"gitlab.com/ZorinArsenij/moneydeal-api-service/internal/config"
	"gitlab.com/ZorinArsenij/moneydeal-api-service/internal/wire"
)

func main() {
	addr := flag.String("addr", "0.0.0.0:8080", "service address")
	storageAddr := flag.String("storageAddr", "0.0.0.0:8082", "storage service address")
	sessionAddr := flag.String("sessionAddr", "0.0.0.0:8081", "session service address")
	flag.Parse()

	cfg := config.Config{
		StorageServiceAddr: *storageAddr,
		SessionServiceAddr: *sessionAddr,
	}

	server, err := wire.InitializeServer(cfg)
	if err != nil {
		log.Fatalf("initialization server failed: %s", err)
	}

	log.Println("server started")
	log.Fatalf("server failed: %s", server.Start(*addr))
}
